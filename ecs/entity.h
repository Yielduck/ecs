#pragma once
#include "utils/cast.h"
#include <pp/find_type.h>
#include <pp/find_property.h>
#include <pp/pick_type.h>
#include <pp/pick_property.h>
namespace ecs
{

template<typename... ComponentList>
struct Entity : public ComponentList...
{
    Entity() = default;
    constexpr Entity(ComponentList &&... component) noexcept
        : ComponentList(utils::forward<ComponentList>(component))...
    {}

    static constexpr pp::type_index componentCount = sizeof...(ComponentList);

    template<pp::type_index i>
    using PickComponent = typename pp::pick_type<i>::template from<ComponentList...>;
    template<typename Component>
    static constexpr pp::type_index componentIdx = pp::find_type<Component>::template in<ComponentList...>;

    template<template<typename> typename Property>
    using PickProperty = typename pp::pick_property<Property>::template from<ComponentList...>;
    template<template<typename> typename Property>
    static constexpr pp::type_index propertyIdx = pp::find_property<Property>::template in<ComponentList...>;

    template<template<typename...> typename ComponentListEater>
    using SubstituteComponentList = ComponentListEater<ComponentList...>;

    template<typename Component>
    Component       &component()            noexcept {return static_cast<Component       &>(*this);}
    template<typename Component>
    Component const &component()      const noexcept {return static_cast<Component const &>(*this);}
};

} // namespace ecs
