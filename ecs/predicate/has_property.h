#pragma once
#include <pp/type_index.h>
namespace ecs::predicate
{

template<template<typename> typename Property>
struct HasProperty
{
    template<typename Entity>
    static constexpr bool value = Entity::template propertyIdx<Property> != pp::not_found;
};

} // namespace ecs::predicate
