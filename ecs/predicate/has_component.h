#pragma once
namespace ecs::predicate
{

template<typename Component>
struct HasComponent
{
    template<typename Entity>
    static constexpr bool value = Entity::template componentIdx<Component> != pp::not_found;
};

} // namespace ecs::predicate
