#pragma once
namespace ecs::predicate
{

struct True
{
    template<typename Entity>
    static constexpr bool value = true;
};

} // namespace ecs::predicate
