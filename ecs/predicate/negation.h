#pragma once
namespace ecs::predicate
{

template<typename Predicate>
struct Negation
{
    template<typename Entity>
    static constexpr bool value = !Predicate::template value<Entity>;
};

} // namespace ecs::predicate
