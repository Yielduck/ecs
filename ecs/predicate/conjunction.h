#pragma once
namespace ecs::predicate
{

template<typename... Predicate>
struct Conjunction
{
    template<typename Entity>
    static constexpr bool value = ((Predicate::template value<Entity>) && ...);
};

} // namespace ecs::predicate
