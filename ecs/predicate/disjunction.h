#pragma once
namespace ecs::predicate
{

template<typename... Predicate>
struct Disjunction
{
    template<typename Entity>
    static constexpr bool value = ((Predicate::template value<Entity>) || ...);
};

} // namespace ecs::predicate
