#pragma once
#include "cast.h"
namespace ecs::utils
{

template<template<typename> typename Property, typename Entity>
auto &pickComponent(Entity &entity) noexcept
{
    using ComponentType = typename detail::remove_reference_t<Entity>::template PickProperty<Property>;
    return entity.template component<ComponentType>();
}

} // namespace ecs::utils
