#pragma once
namespace ecs::utils
{

template<typename T>
struct ref_decay            {using type = T;};
template<typename T>
struct ref_decay<T &>       {using type = T;};
template<typename T>
struct ref_decay<T const &> {using type = T;};
template<typename T>
using ref_decay_t = typename ref_decay<T>::type;

} // namespace ecs::utils
