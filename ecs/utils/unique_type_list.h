#pragma once
namespace ecs::utils
{

template<typename... T>
struct UniqueTypeList : T...
{};

} // namespace ecs::utils
