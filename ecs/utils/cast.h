#pragma once
namespace ecs::utils
{

namespace detail
{

template<typename T>
struct remove_reference         {using type = T;};
template<typename T>
struct remove_reference<T &>    {using type = T;};
template<typename T>
struct remove_reference<T &&>   {using type = T;};
template<typename T>
using remove_reference_t = typename remove_reference<T>::type;

} // namespace detail

template<typename T>
constexpr T &&forward(detail::remove_reference_t<T>  &item) noexcept
{
    return static_cast<T &&>(item);
}
template<typename T>
constexpr T &&forward(detail::remove_reference_t<T> &&item) noexcept
{
    return static_cast<T &&>(item);
}
template<typename T>
constexpr detail::remove_reference_t<T> &&move(T &&item) noexcept
{
    return static_cast<detail::remove_reference_t<T> &&>(item);
}

} // namespace ecs::utils
