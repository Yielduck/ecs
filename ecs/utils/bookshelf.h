#pragma once
#include <algorithm>    // std::nth_element, std::move, std::forward
#include <cassert>      // assert
#include <cstdlib>      // std::malloc, std::realloc, std::free
#include <cstring>      // std::memcpy
#include <type_traits>  // std::is_trivially_copy_constructible, std::is_trivially_move_constructible
namespace ecs::utils
{

/* 
 * this container offers O(1) insertion (in case enough space is reserved), erase and access
 * it works just like the pair std::vector<T>, std::stack<idx_t>
 * stack stores indices, available for insertion in stack
 * on insert, index is taken from stack, on erase it is given back
 *
 * insert/emplace return unique id, which is valid until object is erased
 * this id may be used to retrieve object using operator[] or erase it
 *
 * iterators are invalidated after reserve, shrink, or insert/emplace
 * erase does not invalidate them
 *
 * in case std::is_trivially_move_constructible<T>::value == true,
 * std::realloc is used instead of std::malloc and move constructing
 */
template<typename T, typename uint = unsigned int>
class bookshelf
{
    template<typename U>
    struct iterator_base;

public:
    using value             = T;
    using size_type         = uint;
    using iterator          = iterator_base<T>;
    using const_iterator    = iterator_base<T const>;

                   ~bookshelf   ()               /*implicit noexcept*/;
                    bookshelf   ()                          noexcept;
                    bookshelf   (bookshelf const &)         noexcept;
                    bookshelf   (bookshelf      &&)         noexcept;
    bookshelf &     operator=   (bookshelf const &)         noexcept;
    bookshelf &     operator=   (bookshelf      &&)         noexcept;

    size_type       size        ()                  const   noexcept;
    size_type       capacity    ()                  const   noexcept;
    T &             operator[]  (size_type)                 noexcept;
    T const &       operator[]  (size_type)         const   noexcept;
    bool            alive       (size_type)         const   noexcept;
    T *             pointer     (size_type)                 noexcept;
    T const *       pointer     (size_type)         const   noexcept;

    size_type       insert      (T const &)                 noexcept;
    size_type       insert      (T &&)                      noexcept;
    template<typename... Args>
    size_type       emplace     (Args &&... args)           noexcept;
    void            erase       (size_type)                 noexcept;
    void            erase       (iterator)                  noexcept;
    void            erase       (const_iterator)            noexcept;
    // reference must refer to the object inside container
    void            erase       (T const &)                 noexcept;

    // returns index of the object [that thing that is returned by insert/emplace]
    size_type       idx         (iterator)          const   noexcept;
    size_type       idx         (const_iterator)    const   noexcept;
    // reference must refer to the object inside container
    size_type       idx         (T const &)         const   noexcept;

    void            reserve     (size_type)                 noexcept;
    void            clear       ()                          noexcept;
    void            shrink      ()                          noexcept;

    iterator        begin       ()                          noexcept;
    iterator        end         ()                          noexcept;
    const_iterator  begin       ()                  const   noexcept;
    const_iterator  end         ()                  const   noexcept;
    const_iterator  cbegin      ()                  const   noexcept;
    const_iterator  cend        ()                  const   noexcept;

private:
    T *         m_data;
    bool *      m_alive;

    // works as stack of available indices, m_size is stack head
    size_type * m_idx;

    size_type   m_size;
    size_type   m_capacity;
    size_type   m_end;

    void reallocate(size_type) noexcept;

    template<typename U>
    struct iterator_base
    {
        iterator_base<U> &operator++() noexcept
        {
            while(++ptr != eptr && *(++aptr) == false)
                ;
            return *this;
        }
        U *operator->() const noexcept {return ptr;}
        U &operator*() const noexcept {return *ptr;}
        bool operator!=(iterator_base<U> const &other) const noexcept {return ptr != other.ptr;}
        bool operator==(iterator_base<U> const &other) const noexcept {return ptr == other.ptr;}

    private:
        iterator_base(U * const begin, U * const end, bool const * const alive) noexcept
            : ptr(begin)
            , eptr(end)
            , aptr(alive)
        {
            while(ptr != eptr && *aptr == false)
            {
                ++ptr;
                ++aptr;
            }
        }

        U *ptr;
        U * const eptr;
        bool const *aptr;

        friend class bookshelf<T, uint>;
    };
};

template<typename T, typename uint>
void bookshelf<T, uint>::reallocate(size_type const new_capacity) noexcept
{
    assert(new_capacity >= m_end);
    if constexpr(std::is_trivially_move_constructible<T>::value)
        m_data = static_cast<T *>(std::realloc(m_data, new_capacity * sizeof(T)));
    else
    {
        T * const new_data = static_cast<T *>(std::malloc(new_capacity * sizeof(T)));
        for(size_type i = 0; i < m_end; ++i)
            if(m_alive[i])
            {
                new (new_data + i) T(static_cast<T &&>(m_data[i]));
                (m_data + i)->~T();
            }
        std::free(m_data);
        m_data = new_data;
    }
    assert(new_capacity == 0 ? m_data == nullptr : m_data != nullptr);

    m_alive = static_cast<bool *>(std::realloc(m_alive, new_capacity * sizeof(bool)));
    m_idx = static_cast<size_type *>(std::realloc(m_idx, new_capacity * sizeof(size_type)));
    for(size_type i = m_capacity; i < new_capacity; ++i)
    {
        m_alive[i] = false;
        m_idx[i] = i;
    }
    m_capacity = new_capacity;
}
template<typename T, typename uint>
bookshelf<T, uint>::~bookshelf()
{
    for(size_type i = 0; i < m_end; ++i)
        if(m_alive[i])
            (m_data + i)->~T();
    std::free(m_data);
    std::free(m_alive);
    std::free(m_idx);
}
template<typename T, typename uint>
bookshelf<T, uint>::bookshelf() noexcept
    : m_data(nullptr)
    , m_alive(nullptr)
    , m_idx(nullptr)
    , m_size(0)
    , m_capacity(0)
    , m_end(0)
{}
template<typename T, typename uint>
bookshelf<T, uint>::bookshelf(bookshelf<T, uint> const &other) noexcept
    : m_data(nullptr)
    , m_alive(nullptr)
    , m_idx(nullptr)
    , m_size(0)
    , m_capacity(0)
    , m_end(0)
{
    reallocate(other.m_capacity);
    if constexpr(std::is_trivially_copy_constructible<T>::value)
        std::memcpy(m_data, other.m_data, m_capacity * sizeof(T));
    else
        for(size_type i = 0; i < other.m_end; ++i)
            if(other.m_alive[i])
                new (m_data + i) T(other[i]);
    std::memcpy(m_alive, other.m_alive, m_capacity * sizeof(bool));
    std::memcpy(m_idx, other.m_idx, m_capacity * sizeof(size_type));
    m_size = other.m_size;
    m_end = other.m_end;
}
template<typename T, typename uint>
bookshelf<T, uint>::bookshelf(bookshelf<T, uint> &&other) noexcept
    : m_data(other.m_data)
    , m_alive(other.m_alive)
    , m_idx(other.m_idx)
    , m_size(other.m_size)
    , m_capacity(other.m_capacity)
    , m_end(other.m_end)
{
    new (&other) bookshelf<T, uint>();
}
template<typename T, typename uint>
bookshelf<T, uint> &bookshelf<T, uint>::operator=(bookshelf<T, uint> const &other) noexcept
{
    if(this != &other)
    {
        this->~bookshelf<T, uint>();
        new (this) bookshelf<T, uint>(other);
    }
    return *this;
}
template<typename T, typename uint>
bookshelf<T, uint> &bookshelf<T, uint>::operator=(bookshelf<T, uint> &&other) noexcept
{
    if(this != &other)
    {
        this->~bookshelf<T, uint>();
        new (this) bookshelf<T, uint>(static_cast<bookshelf<T, uint> &&>(other));
    }
    return *this;
}
template<typename T, typename uint>
uint bookshelf<T, uint>::size() const noexcept
{
    return m_size;
}
template<typename T, typename uint>
uint bookshelf<T, uint>::capacity() const noexcept
{
    return m_capacity;
}
template<typename T, typename uint>
T &bookshelf<T, uint>::operator[](size_type const i) noexcept
{
    assert(alive(i));
    return m_data[i];
}
template<typename T, typename uint>
T const &bookshelf<T, uint>::operator[](size_type const i) const noexcept
{
    assert(alive(i));
    return m_data[i];
}
template<typename T, typename uint>
T *bookshelf<T, uint>::pointer(size_type const i) noexcept
{
    return alive(i) ? m_data + i : nullptr;
}
template<typename T, typename uint>
T const *bookshelf<T, uint>::pointer(size_type const i) const noexcept
{
    return alive(i) ? m_data + i : nullptr;
}
template<typename T, typename uint>
bool bookshelf<T, uint>::alive(size_type const i) const noexcept
{
    return i < m_end && m_alive[i];
}
template<typename T, typename uint>
uint bookshelf<T, uint>::insert(T const &item) noexcept
{
    return emplace(item);
}
template<typename T, typename uint>
uint bookshelf<T, uint>::insert(T &&item) noexcept
{
    return emplace(std::move(item));
}
template<typename T, typename uint>
template<typename... Args>
uint bookshelf<T, uint>::emplace(Args &&... args) noexcept
{
    if(m_size == m_capacity)
        reallocate(m_size == 0 ? 1 : m_size * 2);
    size_type const idx = m_idx[m_size++];
    m_alive[idx] = true;
    new (m_data + idx) T(std::forward<Args>(args)...);
    m_end = m_end > idx ? m_end : idx + 1;
    return idx;
}
template<typename T, typename uint>
void bookshelf<T, uint>::erase(size_type const idx) noexcept
{
    assert(idx < m_end);
    assert(m_alive != nullptr && m_idx != nullptr);
    assert(m_alive[idx]);
    (m_data + idx)->~T();
    m_alive[idx] = false;
    if(idx + 1 == m_end)
        while(--m_end != 0 && m_alive[m_end - 1] == false)
            ;
    m_idx[--m_size] = idx;
}
template<typename T, typename uint>
typename bookshelf<T, uint>::size_type bookshelf<T, uint>::idx(iterator const iter) const noexcept
{
    assert(iter.ptr >= m_data && iter.ptr < m_data + m_end);
    return static_cast<size_type>(iter.ptr - m_data);
}
template<typename T, typename uint>
typename bookshelf<T, uint>::size_type bookshelf<T, uint>::idx(const_iterator const iter) const noexcept
{
    assert(iter.ptr >= m_data && iter.ptr < m_data + m_end);
    return static_cast<size_type>(iter.ptr - m_data);
}
template<typename T, typename uint>
typename bookshelf<T, uint>::size_type bookshelf<T, uint>::idx(T const &item) const noexcept
{
    assert(&item >= m_data && &item < m_data + m_end);
    return static_cast<size_type>(&item - m_data);
}
template<typename T, typename uint>
void bookshelf<T, uint>::erase(iterator const iter) noexcept
{
    erase(idx(iter));
}
template<typename T, typename uint>
void bookshelf<T, uint>::erase(const_iterator const iter) noexcept
{
    erase(idx(iter));
}
template<typename T, typename uint>
void bookshelf<T, uint>::erase(T const &item) noexcept
{
    erase(idx(item));
}
template<typename T, typename uint>
void bookshelf<T, uint>::reserve(size_type const cap) noexcept
{
    if(cap > m_capacity)
        reallocate(cap);
}
template<typename T, typename uint>
void bookshelf<T, uint>::clear() noexcept
{
    if(m_capacity != 0)
    {
        for(size_type i = 0; i < m_end; ++i)
            if(m_alive[i])
                (m_data + i)->~T();
        m_size = 0;
        m_end = 0;
        for(size_type i = 0; i < m_capacity; ++i)
        {
            m_alive[i] = false;
            m_idx[i] = i;
        }
    }
}
template<typename T, typename uint>
void bookshelf<T, uint>::shrink() noexcept
{
    if(m_capacity == 0)
        return;
    if(m_size != m_end)
        std::nth_element(m_idx + m_size, m_idx + m_end - 1, m_idx + m_capacity);
    reallocate(m_end);
}
template<typename T, typename uint>
typename bookshelf<T, uint>::iterator bookshelf<T, uint>::begin() noexcept
{
    return iterator{m_data, m_data + m_end, m_alive};
}
template<typename T, typename uint>
typename bookshelf<T, uint>::iterator bookshelf<T, uint>::end() noexcept
{
    return iterator{m_data + m_end, m_data + m_end, m_alive};
}
template<typename T, typename uint>
typename bookshelf<T, uint>::const_iterator bookshelf<T, uint>::begin() const noexcept
{
    return const_iterator{m_data, m_data + m_end, m_alive};
}
template<typename T, typename uint>
typename bookshelf<T, uint>::const_iterator bookshelf<T, uint>::end() const noexcept
{
    return const_iterator{m_data + m_end, m_data + m_end, m_alive};
}
template<typename T, typename uint>
typename bookshelf<T, uint>::const_iterator bookshelf<T, uint>::cbegin() const noexcept
{
    return begin();
}
template<typename T, typename uint>
typename bookshelf<T, uint>::const_iterator bookshelf<T, uint>::cend() const noexcept
{
    return end();
}

} // namespace ecs::utils
