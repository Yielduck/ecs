#pragma once
#include <type_traits>
#include <pp/find_property.h>
#include <pp/pick_property.h>
namespace ecs::property
{

template<template<typename> typename Base>
struct IsCRTPDerivedFrom
{
    template<typename Derived>
    using type = std::is_base_of<Base<Derived>, Derived>;
};

template<template<typename> typename Base>
struct FindCRTPDerivedFrom
{
    template<typename... U>
    static constexpr pp::type_index in = pp::find_property<IsCRTPDerivedFrom<Base>::template type>::template in<U...>;
};

template<template<typename> typename Base>
struct PickCRTPDerivedFrom
{
    template<typename... U>
    using from = typename pp::pick_property<IsCRTPDerivedFrom<Base>::template type>::template from<U...>;
};

} // namespace ecs::property
