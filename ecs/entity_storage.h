#pragma once
#include "entity.h"
namespace ecs
{

template<template<typename> typename StorageT, typename... EntityList>
struct EntityStorage : private StorageT<EntityList>...
{
    template<typename Entity>
    using Storage = StorageT<Entity>;

    static constexpr pp::type_index entityCount = sizeof...(EntityList);
    template<pp::type_index i>
    using PickEntity = typename pp::pick_type<i>::template from<EntityList...>;
    template<typename Entity>
    static constexpr pp::type_index entityIdx = pp::find_type<Entity>::template in<EntityList...>;

    template<template<typename> typename Property>
    using PickProperty = typename pp::pick_property<Property>::template from<EntityList...>;
    template<template<typename> typename Property>
    static constexpr pp::type_index propertyIdx = pp::find_property<Property>::template in<EntityList...>;

    template<template<typename...> typename EntityListEater>
    using SubstituteEntityList = EntityListEater<EntityList...>;

    template<typename Entity>
    Storage<Entity>       &storage()            noexcept {return static_cast<Storage<Entity>       &>(*this);}
    template<typename Entity>
    Storage<Entity> const &storage()      const noexcept {return static_cast<Storage<Entity> const &>(*this);}
};

} // namespace ecs
