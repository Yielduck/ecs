#pragma once
#include "predicate/true.h"
#include "utils/unique_type_list.h"
#include <pp/type_index.h>
#include <cassert>
namespace ecs
{

template<typename EntityStorage> // may be const
struct EntityStorageTraits
{
    using EntityList = typename EntityStorage::template SubstituteEntityList<utils::UniqueTypeList>;

    template<typename Predicate = ecs::predicate::True, typename Visitor>
    // for every Entity in EntityList: if Predicate::value<Entity> is true
    // then Visitor must implement void operator()(EntityStorage::Storage<Entity> [const] &)
    static void visit(EntityStorage * const pEntityStorage, pp::type_index const entityIdx, Visitor &&visitor)
    {
        assert(entityIdx < EntityStorage::entityCount);
        assert(pEntityStorage != nullptr);
        visitImpl<Predicate>(pEntityStorage, utils::forward<Visitor>(visitor), entityIdx, static_cast<EntityList *>(nullptr));
    }

    template<typename Predicate = ecs::predicate::True, typename Visitor>
    static void forEachStorage(EntityStorage * const pEntityStorage, Visitor &&visitor)
    {
        assert(pEntityStorage != nullptr);
        forEachStorageImpl<Predicate>(pEntityStorage, utils::forward<Visitor>(visitor), static_cast<EntityList *>(nullptr));
    }

private:
    template<typename Visitor>
    using VisitorFunc = void(Visitor &&, EntityStorage *);
    template<typename Predicate, typename Visitor, typename... Entity>
    static void visitImpl(EntityStorage * const pEntityStorage, Visitor &&visitor, pp::type_index const entityIdx, utils::UniqueTypeList<Entity...> *)
    {
        static constexpr VisitorFunc<Visitor> *visitFunc[sizeof...(Entity)] =
        {
            +[](Visitor &&visit, [[maybe_unused]] EntityStorage * const pStorage) -> void
            {
                if constexpr(Predicate::template value<Entity>)
                    visit(pStorage->template storage<Entity>());
                else
                    assert(0);
            }...
        };
        visitFunc[entityIdx](utils::forward<Visitor>(visitor), pEntityStorage);
    }
    template<typename Predicate, typename Visitor, typename... Entity>
    static void forEachStorageImpl(EntityStorage * const pEntityStorage, Visitor &&visitor, utils::UniqueTypeList<Entity...> *)
    {
        (
            [](Visitor &&visit, [[maybe_unused]] EntityStorage * const pStorage) -> void
            {
                if constexpr(Predicate::template value<Entity>)
                    visit(pStorage->template storage<Entity>());
            }(utils::forward<Visitor>(visitor), pEntityStorage),...
        );
    }
};

} // namespace ecs
