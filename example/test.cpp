#include <ecs/entity_storage.h>
#include <ecs/entity_storage_traits.h>
#include <ecs/predicate/has_component.h>
#include <ecs/property/is_crtp_derived_from.h>
#include <ecs/utils/bookshelf.h>
#include <iostream>

template<typename T>
struct SimpleComponent
{
    T value;
    SimpleComponent() = default;
    constexpr SimpleComponent(T const &v) noexcept
        : value(v)
    {}
    operator T() const noexcept {return value;}
};
using Int = SimpleComponent<int>;
using Float = SimpleComponent<float>;
using Double = SimpleComponent<double>;
using Char = SimpleComponent<char>;

using Entity1 = ecs::Entity<Int, Float>;
using Entity2 = ecs::Entity<Int, Double>;
using Entity3 = ecs::Entity<Char>;

template<typename T>
using StorageT = ecs::utils::bookshelf<T>;

using ES = ecs::EntityStorage<StorageT, Entity1, Entity2, Entity3>;

template<typename EntityStorage>
struct PrintIntSystem
{
    using Traits = ecs::EntityStorageTraits<EntityStorage const>;
    EntityStorage const * const pStorage;

    void printAll() const
    {
        Traits::template forEachStorage<ecs::predicate::HasComponent<Int>>
        (
            pStorage,
            [](auto const &storage) -> void
            {
                for(auto const &entity : storage)
                    std::cout << static_cast<Int>(entity) << " ";
                std::cout << std::endl;
            }
        );
    }
    void print(pp::type_index const entityIdx) const
    {
        Traits::template visit<ecs::predicate::HasComponent<Int>>
        (
            pStorage,
            entityIdx,
            [](auto const &storage) -> void
            {
                for(auto const &entity : storage)
                    std::cout << static_cast<Int>(entity) << " ";
                std::cout << std::endl;
            }
        );
    }
};

int main()
{
    ES entityStorage;
    PrintIntSystem<ES const> const intPrinter = {&entityStorage};
    ES::Storage<Entity1> &es1 = entityStorage.storage<Entity1>();
    ES::Storage<Entity2> &es2 = entityStorage.storage<Entity2>();
    ES::Storage<Entity3> &es3 = entityStorage.storage<Entity3>();
    for(int i = 0; i < 5; ++i)
        es1.emplace(i, 0.f);
    for(int i = 10; i < 15; ++i)
        es2.emplace(i, -1.);
    for(char c = '0'; c <= '9'; ++c)
        es3.emplace(c);

    for(Entity1 const &e : es1)
        if(static_cast<Int>(e) > 2)
            es1.erase(e);
    es1.emplace(0, 0.f);
    es2.erase(3);
    es2.erase(2);
    es2.emplace(0, 0.);
    intPrinter.printAll();
    intPrinter.print(ES::entityIdx<Entity1>);
    intPrinter.print(1);
}
